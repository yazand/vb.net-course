Module GuessingGame

		
	Sub Main()
	
		Dim PlayAgain As Boolean
		
		Randomize() 'Used to randomize the function Rnd() used later. Randomize has to be called at least once. And it is customary to call it at the start of the program.
		
		PrintIntroduction() 'We are a calling a subroutine that we created
		
		Do
			
			PlayGame()
			PlayAgain = AskToPlayAgain() 'AskToPlayAgain is a function defined below
			
			If PlayAgain Then 
				Console.WriteLine("---------------")
				Console.WriteLine("I have a new number in my mind that is between 1 and 20.")
			End If
			
		Loop While PlayAgain
	
		Console.WriteLine("Have a nice day! Bye.")
		Console.ReadKey() 'We used Console.ReadKey() to wait for a key before exiting the window
		
	End Sub
	
	
	Sub PrintIntroduction()
	
		Console.WriteLine("Guess My Number") 'Print an introduction to the program
		Console.WriteLine("---------------")
		
		Console.WriteLine("I have a number in my mind. It is a number between 1 and 20.") 'Print game instructions
		Console.WriteLine("Try to guess my number in a maximum of four trials.")
		Console.WriteLine("---------------")
		
	End Sub
	
	
	Sub PlayGame()
	
		Dim Number As Integer = Math.Round(Rnd() * 19 + 1) 'This will produce a number between 1 and 10
		Const NumberOfTrials As Integer = 4 'This was not explained. But it declares NumberOfTrials as an integer constant.
		
		Dim GuessedSuccessfully As Boolean = False 'When this is true, I will know that the guess was correct.
		
		For I as Integer = 1 To NumberOfTrials 'Four is the max number of trials
		
			If I = NumberOfTrials Then Console.WriteLine("Beware! This is your last trial.") 'Remind the player that this is his last trial if it is.
			
			Console.Write("Enter your guess: ")		
			
			Dim Guess As Integer = Console.ReadLine() 'Read the guess
			
			If Guess = Number Then
				GuessedSuccessfully = True
				Exit For
			End If
			
			If Not I = NumberOfTrials Then 'If he already used his last trial. There is no need to tell him the following information.
				If Guess > Number Then
					Console.WriteLine("The number in my mind is smaller than " & Guess & ".") 'Here we used concatenation of more than one string together
				Else 
					Console.WriteLine("The number in my mind is larger than " & Guess & ".")
				End If
			End If
			
		Next I
		
		If GuessedSuccessfully Then
			Console.WriteLine("Good guess! You win.")
		Else
			Console.WriteLine("Bad luck! You lose. The number was " & Number & ".")
		End If
		
	End Sub
	
	
	Function AskToPlayAgain() As Boolean 'This is a function that returns a boolean (true / false)
	
		Console.WriteLine("Do you want to play again? [Y/N]")
		
		Do
		
			Dim Input As String = Console.ReadLine()
			Input = LCase(Input) 'LCase was not explained. It is a function that returns the lower-case representation of a string.
			'For example, LCase("ABCde") --> "abcde"
			'This will enable testing only for the lower-case letters in the following if
			
			If Input = "y" Then
				Return True 'By returning, I will exit the whole function. And so will exit the Do Loop without using Exit Do.
			ElseIf Input = "n" Then
				Return False
			Else
				Console.WriteLine("Sorry. Your input is invalid. Please either use Y or N.")
			End If
			
		Loop
		
	End Function
	
	
End Module
