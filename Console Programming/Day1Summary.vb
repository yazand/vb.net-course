Module DayOneSummary
	
	Sub Main()
	
		'We can put comments in code using the single quote like in this comment.
		
		Console.WriteLine("Hello World!") 'We use Console.WriteLine to print any string to the console

		Console.Write("This ") 'We can use Console.Write to print a string wihout moving to the next line
		Console.WriteLine("and this lie on the same line")
		
		'The previous two lines will print the following on the same line
		'This and this lie on the same line
		
		'---------------------------------------------------------------------------------------------------
		
		'We can declare a variable using the following syntax
		
		Dim VarName As Integer 'Here we declare a new variable called VarName. It is of type integer.
		'I cannot use a variable before declaring it.
		
		'Uncommenting the following code will create a compilation error:
		'X = 5
		'A compilation error will occur because X was not declared before used
		
		'---------------------------------------------------------------------------------------------------
		
		'Variables can have different values throught the life of your process.
		VarName = 5
		VarName = VarName + 10
		VarName = 0
		
		'I can also take input from the console and store it in a variable.
		Dim InputFromUser As String
		InputFromUser = Console.ReadLine()
		
		'---------------------------------------------------------------------------------------------------
		
		'Sometimes you will want different code to run depending on some specific conditions
		'For example, you may want to set a flag (boolean) to true if some number is larger than 10
		'We use the if control
		
		Dim LargerFlag as Boolean 
		
		If VarName > 10 Then
			LargerFlag = True 'Note that I used a tab before starting this line to indicate that LargerFlag = True is part of the if
		Else
			LargerFlag = False
		End If
		
		'Sometimes I would like to use a shorter form of the if control
		'Note: This is a separate example
		If VarName = 5 Then LargerFlag = True
		
		'Or I could even use
		If VarName <= 3 Then LargerFlag = True Else LargerFlag = False
		
		'Else is optional in either case
		'But this form must not have an 'End If' afterwards
		
		'---------------------------------------------------------------------------------------------------
		
		'We used the for loop to iterate several times over the same code
		'For example, to print the numbers from 1 to 10, we use
		Console.WriteLine("Printing the numbers from 1 to 10")
		For I As Integer = 1 to 10
			Console.WriteLine(I)
		Next I 'the I after Next is optional
		
		'We could have used it to print the even numbers only
		Console.WriteLine("Printing the even numbers from 2 to 10")
		For I As Integer = 2 to 10 Step 2
			Console.WriteLine(I)
		Next I
		
		'And we can make it go in reverse
		Console.WriteLine("Printing the numbers from 10 to 1")
		For I As Integer = 10 To 1 Step -1
			Console.WriteLine(I)
		Next I
		
		'Note that using I outside the for loop will result in a compilation error. I will explain this later.
		
	End Sub
	
End Module

