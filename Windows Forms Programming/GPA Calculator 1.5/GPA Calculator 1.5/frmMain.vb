﻿Public Class frmMain

    Dim TotalHours As Integer = 0 'This variable contains the total hours.
    Dim TotalPoints As Double = 0 'This variable contains the total points.

    Private Sub btnAdd_Click(sender As System.Object, e As System.EventArgs) Handles btnAdd.Click

        TotalHours += CInt(txtHours.text) 'Add to the global variable total hours the number of hours in the textbox txtHours
        TotalPoints += CDbl(txtHours.text) * getMark(cmbGrade.Text) 'Add to the global variable TotalPoints the points for this subject.
	'The points are equal to number of hours * grade out of 4

        lblHours.Text = TotalHours & " hours"
        lblGPA.Text = (TotalPoints / TotalHours).ToString("0.00") & "/4.00" 'for a description of ToString, check the comments in the first GPA Calculator project.

        lstSubjects.Items.Add("You have an " & cmbGrade.Text & " in the " & txtHours.Text & "-credit hour subject, " & txtName.Text & ".")
	'add to the listbox lstSubjects an item. It will be in this format:
	'You have an A in the 3-credit hour subject, Calculus.

        txtHours.Text = ""
        txtName.Text = ""

        txtName.Focus() 'This sets the focus back to txtName to enter new input in it. See what happens to understand it better.

    End Sub

    Function GetMark(ByVal Grade As String) As Double
        Select Case Grade
            Case "A"
                Return 4.0
            Case "B"
                Return 3.0
            Case "C"
                Return 2.0
            Case "D"
                Return 1.0
            Case "F"
                Return 0.0
            Case Else
                MsgBox("Invalid input!", MsgBoxStyle.Exclamation)
                Return 0.0
        End Select
    End Function

    Private Sub btnClear_Click(sender As System.Object, e As System.EventArgs) Handles btnClear.Click

        lstSubjects.Items.Clear() 'clears the listbox of all items

        txtHours.Text = ""
        txtName.Text = ""

	TotalHours = 0 'must reset TotalHours to zero to start calculating a new average
	TotalPoints = 0 'same as above

    End Sub

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
	'I want to confirm that the user wants to exit. A message box can be used as following:
        If MsgBox("Are you sure you want to exit?", MsgBoxStyle.Question Or MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then Close()
    End Sub

End Class
