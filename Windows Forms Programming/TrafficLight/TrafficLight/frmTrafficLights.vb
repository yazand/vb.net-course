﻿Public Class frmTrafficLights

    Dim CurrentLight As Light = Light.Red 'We will start with the red light on.

    Private Sub tmrChangeLight_Tick(sender As System.Object, e As System.EventArgs) Handles tmrChangeLight.Tick

        CurrentLight = LightControl.GetNextLight(CurrentLight) 'Set the color of light to the next one
        ShowLight(CurrentLight) 'Update the picture boxes. Change their colors.

        tmrChangeLight.Interval = LightControl.GetLightInterval(CurrentLight) 'Set the interval for the picture box for the current light.

    End Sub

    Private Sub ShowLight(ByVal Light As Light)

        'This subroutine only shows the light for the specified one, and blacks out all others.

        Select Case Light
            Case LightControl.Light.Red
                picRed.BackColor = Color.Red
                picYellow.BackColor = Color.Black
                picGreen.BackColor = Color.Black
            Case LightControl.Light.Yellow
                picRed.BackColor = Color.Black
                picYellow.BackColor = Color.Yellow
                picGreen.BackColor = Color.Black
            Case LightControl.Light.Green
                picRed.BackColor = Color.Black
                picYellow.BackColor = Color.Black
                picGreen.BackColor = Color.Green
        End Select

    End Sub

End Class