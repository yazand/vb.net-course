﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTrafficLights
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTrafficLights))
        Me.picRed = New System.Windows.Forms.PictureBox()
        Me.picYellow = New System.Windows.Forms.PictureBox()
        Me.picGreen = New System.Windows.Forms.PictureBox()
        Me.lblSteps = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tmrChangeLight = New System.Windows.Forms.Timer(Me.components)
        CType(Me.picRed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picYellow, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picGreen, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picRed
        '
        Me.picRed.BackColor = System.Drawing.Color.Red
        Me.picRed.Location = New System.Drawing.Point(12, 12)
        Me.picRed.Name = "picRed"
        Me.picRed.Size = New System.Drawing.Size(80, 80)
        Me.picRed.TabIndex = 0
        Me.picRed.TabStop = False
        '
        'picYellow
        '
        Me.picYellow.BackColor = System.Drawing.Color.Yellow
        Me.picYellow.Location = New System.Drawing.Point(12, 98)
        Me.picYellow.Name = "picYellow"
        Me.picYellow.Size = New System.Drawing.Size(80, 80)
        Me.picYellow.TabIndex = 1
        Me.picYellow.TabStop = False
        '
        'picGreen
        '
        Me.picGreen.BackColor = System.Drawing.Color.Lime
        Me.picGreen.Location = New System.Drawing.Point(12, 184)
        Me.picGreen.Name = "picGreen"
        Me.picGreen.Size = New System.Drawing.Size(80, 80)
        Me.picGreen.TabIndex = 2
        Me.picGreen.TabStop = False
        '
        'lblSteps
        '
        Me.lblSteps.Location = New System.Drawing.Point(98, 12)
        Me.lblSteps.Name = "lblSteps"
        Me.lblSteps.Size = New System.Drawing.Size(312, 252)
        Me.lblSteps.TabIndex = 3
        Me.lblSteps.Text = resources.GetString("lblSteps.Text")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(98, 196)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(95, 29)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Project:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(179, 219)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(231, 45)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Traffic Lights"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'tmrChangeLight
        '
        Me.tmrChangeLight.Enabled = True
        '
        'frmTrafficLights
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(414, 271)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblSteps)
        Me.Controls.Add(Me.picGreen)
        Me.Controls.Add(Me.picYellow)
        Me.Controls.Add(Me.picRed)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmTrafficLights"
        Me.Text = "Project: Traffic Lights"
        CType(Me.picRed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picYellow, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picGreen, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents picRed As System.Windows.Forms.PictureBox
    Friend WithEvents picYellow As System.Windows.Forms.PictureBox
    Friend WithEvents picGreen As System.Windows.Forms.PictureBox
    Friend WithEvents lblSteps As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tmrChangeLight As System.Windows.Forms.Timer
End Class
