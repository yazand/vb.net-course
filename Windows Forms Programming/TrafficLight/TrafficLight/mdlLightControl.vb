﻿Module LightControl

    Enum Light
        Red = 0
        Yellow = 1
        Green = 2
    End Enum

    Function GetLightInterval(ByVal Light As Light) As Integer

        Select Case Light
            Case LightControl.Light.Red
                Return 5000
            Case LightControl.Light.Yellow
                Return 1000
            Case LightControl.Light.Green
                Return 2000
            Case Else
                'This code path must not be reached normally.
                MsgBox("Error: Behaviour not expected in GetLightInterval().", MsgBoxStyle.Exclamation) 'So let a message box inform me that I reached it.
                Return 0
        End Select

    End Function

    Function GetNextLight(ByVal CurrentLight As Light) As Light

        Select Case CurrentLight
            Case Light.Red
                Return Light.Yellow
            Case Light.Yellow
                Return Light.Green
            Case Light.Green
                Return Light.Red
            Case Else
                'This code path must not be reached normally.
                MsgBox("Error: Behaviour not expected in GetNextLight().", MsgBoxStyle.Exclamation) 'So let a message box inform me that I reached it.
                Return Light.Red
        End Select

    End Function

End Module
