﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMixer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.trkRed = New System.Windows.Forms.TrackBar()
        Me.trkGreen = New System.Windows.Forms.TrackBar()
        Me.trkBlue = New System.Windows.Forms.TrackBar()
        Me.picGreen = New System.Windows.Forms.PictureBox()
        Me.picRed = New System.Windows.Forms.PictureBox()
        Me.picBlue = New System.Windows.Forms.PictureBox()
        Me.picMix = New System.Windows.Forms.PictureBox()
        CType(Me.trkRed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.trkGreen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.trkBlue, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picGreen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBlue, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picMix, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(27, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Red"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 57)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(36, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Green"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 105)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(28, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Blue"
        '
        'trkRed
        '
        Me.trkRed.Location = New System.Drawing.Point(57, 9)
        Me.trkRed.Maximum = 255
        Me.trkRed.Name = "trkRed"
        Me.trkRed.Size = New System.Drawing.Size(104, 42)
        Me.trkRed.TabIndex = 3
        Me.trkRed.TickFrequency = 20
        '
        'trkGreen
        '
        Me.trkGreen.Location = New System.Drawing.Point(57, 57)
        Me.trkGreen.Maximum = 255
        Me.trkGreen.Name = "trkGreen"
        Me.trkGreen.Size = New System.Drawing.Size(104, 42)
        Me.trkGreen.TabIndex = 4
        Me.trkGreen.TickFrequency = 20
        '
        'trkBlue
        '
        Me.trkBlue.Location = New System.Drawing.Point(57, 105)
        Me.trkBlue.Maximum = 255
        Me.trkBlue.Name = "trkBlue"
        Me.trkBlue.Size = New System.Drawing.Size(104, 42)
        Me.trkBlue.TabIndex = 5
        Me.trkBlue.TickFrequency = 20
        '
        'picGreen
        '
        Me.picGreen.Location = New System.Drawing.Point(167, 57)
        Me.picGreen.Name = "picGreen"
        Me.picGreen.Size = New System.Drawing.Size(20, 18)
        Me.picGreen.TabIndex = 6
        Me.picGreen.TabStop = False
        '
        'picRed
        '
        Me.picRed.Location = New System.Drawing.Point(167, 9)
        Me.picRed.Name = "picRed"
        Me.picRed.Size = New System.Drawing.Size(20, 18)
        Me.picRed.TabIndex = 7
        Me.picRed.TabStop = False
        '
        'picBlue
        '
        Me.picBlue.Location = New System.Drawing.Point(167, 105)
        Me.picBlue.Name = "picBlue"
        Me.picBlue.Size = New System.Drawing.Size(20, 20)
        Me.picBlue.TabIndex = 8
        Me.picBlue.TabStop = False
        '
        'picMix
        '
        Me.picMix.Location = New System.Drawing.Point(15, 153)
        Me.picMix.Name = "picMix"
        Me.picMix.Size = New System.Drawing.Size(172, 50)
        Me.picMix.TabIndex = 9
        Me.picMix.TabStop = False
        '
        'frmMixer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(206, 217)
        Me.Controls.Add(Me.picMix)
        Me.Controls.Add(Me.picBlue)
        Me.Controls.Add(Me.picRed)
        Me.Controls.Add(Me.picGreen)
        Me.Controls.Add(Me.trkBlue)
        Me.Controls.Add(Me.trkGreen)
        Me.Controls.Add(Me.trkRed)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmMixer"
        Me.Text = "Project: Colors Mixer"
        CType(Me.trkRed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.trkGreen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.trkBlue, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picGreen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBlue, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picMix, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents trkRed As System.Windows.Forms.TrackBar
    Friend WithEvents trkGreen As System.Windows.Forms.TrackBar
    Friend WithEvents trkBlue As System.Windows.Forms.TrackBar
    Friend WithEvents picGreen As System.Windows.Forms.PictureBox
    Friend WithEvents picRed As System.Windows.Forms.PictureBox
    Friend WithEvents picBlue As System.Windows.Forms.PictureBox
    Friend WithEvents picMix As System.Windows.Forms.PictureBox

End Class
