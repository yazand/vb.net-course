﻿Public Class frmMixer

    Private Sub ColorChanged(sender As System.Object, e As System.EventArgs) Handles trkRed.Scroll, trkGreen.Scroll, trkBlue.Scroll

	'Notice that I exchanged the HScrollBar with a Trackbar
	'They behave similarily in our project

	'There are three picture boxes. One for each color.
	'Each picture box will have a color as the value of the corresponding trackbar in that color.
	'A forth picture box will mix all colors together.
        picRed.BackColor = Color.FromArgb(Me.trkRed.Value, 0, 0)
        picGreen.BackColor = Color.FromArgb(0, Me.trkGreen.Value, 0)
        picBlue.BackColor = Color.FromArgb(0, 0, Me.trkBlue.Value)
        picMix.BackColor = Color.FromArgb(Me.trkRed.Value, Me.trkGreen.Value, Me.trkBlue.Value)

    End Sub

End Class
