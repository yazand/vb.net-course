﻿Public Class Car

    Private _Color As Color 'we use private instead of dim to clarify that this variable is inaccessible from outside this class
    Private _MaxSpeed As Double 'The underscore is only a convention (not a requirement) used with private members
    Private _Speed As Double

    Private _Box As PictureBox 'picture box that represents the car

    ReadOnly Property Color As Color 'define a property that can only be read of type color
        Get
            Return _Color
        End Get
    End Property

    ReadOnly Property Speed As Double
        Get
            Return _Speed
        End Get
    End Property

    ReadOnly Property MaxSpeed As Double
        Get
            Return _MaxSpeed
        End Get
    End Property

    Sub New(ByVal Color As Color, ByVal MaxSpeed As Double) 'Constructor. To create a new object from the class Car, I need to provide parameters for one of constructors.
        MyClass.New(Color, MaxSpeed, 0) 'MyClass refers to this class. As to not repeat code, I will call the other constructor with suitable parameters.
    End Sub

    Sub New(ByVal Color As Color, ByVal MaxSpeed As Double, ByVal CurrentSpeed As Double)

        _Color = Color
        _MaxSpeed = MaxSpeed
        _Speed = CurrentSpeed

        _Box = New PictureBox() 'create a new picture box
        _Box.Left = 10
        _Box.Top = Rnd() * frmCars.Height
        _Box.BackColor = Color

        _Box.Width = 60
        _Box.Height = 20

        frmCars.Controls.Add(_Box) 'add the new picture box to the controls of the form

    End Sub

    Sub AccelerateRight(ByVal SpeedIncrease As Double)
        _Speed += SpeedIncrease
    End Sub

    Sub AccelerateLeft(ByVal SpeedIncrease As Double)
        AccelerateRight(-SpeedIncrease)
    End Sub

    Sub Move() 'this subroutine will be called when updating the location of the picture box is needed (i.e. each frame)
        _Box.Left += _Speed
    End Sub

End Class
