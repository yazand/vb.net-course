﻿Public Class frmCars

    Dim Cars(-1) As Car 'create an empty array of type Car
    Dim MyCar As Car 'declare MyCar as Car also. MyCar represents the red car that you can control.

    Private Sub frmCars_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

	'This event handles the event when a key was pressed down.

        Select Case e.KeyCode 'e.KeyCode refers to the key that was pressed down.
            Case Keys.A
                MyCar.AccelerateLeft(5) 'accelerate my car left
            Case Keys.D
                MyCar.AccelerateRight(5) 'accelerate my car right
            Case Keys.Space
                ReDim Preserve Cars(Cars.Length) 'resize the Cars array preserving previous elements. This will increase the size of the array by one.
                Cars(Cars.Length - 1) = New Car(Color.FromArgb(Rnd() * 255, Rnd() * 255, Rnd() * 255), 20, Rnd() * 10) 'initialise a new car
        End Select
        
    End Sub

    Private Sub frmCars_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        MyCar = New Car(Color.Red, 50, 0) 'create the car that you own
        tmrFrame.Enabled = True 'enable the timer to update the positions of the picture boxes in a timely matter.
    End Sub

    Private Sub tmrFrame_Tick(sender As System.Object, e As System.EventArgs) Handles tmrFrame.Tick

	'move all cars
        For Each C As Car In Cars
            C.Move()
        Next

        MyCar.Move()

    End Sub
End Class
