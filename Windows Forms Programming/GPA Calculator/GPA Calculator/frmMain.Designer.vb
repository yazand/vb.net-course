﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cmbSub1Grade = New System.Windows.Forms.ComboBox()
        Me.txtSub1Hours = New System.Windows.Forms.TextBox()
        Me.txtSub2Hours = New System.Windows.Forms.TextBox()
        Me.txtSub3Hours = New System.Windows.Forms.TextBox()
        Me.grpResults = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnCalculate = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.lblHours = New System.Windows.Forms.Label()
        Me.lblGPA = New System.Windows.Forms.Label()
        Me.cmbSub2Grade = New System.Windows.Forms.ComboBox()
        Me.cmbSub3Grade = New System.Windows.Forms.ComboBox()
        Me.grpResults.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Subject 1"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 57)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(52, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Subject 2"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 83)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Subject 3"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(67, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(36, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Grade"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(190, 9)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Hours"
        '
        'cmbSub1Grade
        '
        Me.cmbSub1Grade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSub1Grade.FormattingEnabled = True
        Me.cmbSub1Grade.Items.AddRange(New Object() {"A", "B", "C", "D", "F"})
        Me.cmbSub1Grade.Location = New System.Drawing.Point(70, 28)
        Me.cmbSub1Grade.Name = "cmbSub1Grade"
        Me.cmbSub1Grade.Size = New System.Drawing.Size(121, 21)
        Me.cmbSub1Grade.TabIndex = 5
        '
        'txtSub1Hours
        '
        Me.txtSub1Hours.Location = New System.Drawing.Point(193, 28)
        Me.txtSub1Hours.Name = "txtSub1Hours"
        Me.txtSub1Hours.Size = New System.Drawing.Size(121, 20)
        Me.txtSub1Hours.TabIndex = 6
        '
        'txtSub2Hours
        '
        Me.txtSub2Hours.Location = New System.Drawing.Point(193, 54)
        Me.txtSub2Hours.Name = "txtSub2Hours"
        Me.txtSub2Hours.Size = New System.Drawing.Size(121, 20)
        Me.txtSub2Hours.TabIndex = 8
        '
        'txtSub3Hours
        '
        Me.txtSub3Hours.Location = New System.Drawing.Point(193, 80)
        Me.txtSub3Hours.Name = "txtSub3Hours"
        Me.txtSub3Hours.Size = New System.Drawing.Size(121, 20)
        Me.txtSub3Hours.TabIndex = 10
        '
        'grpResults
        '
        Me.grpResults.Controls.Add(Me.lblHours)
        Me.grpResults.Controls.Add(Me.lblGPA)
        Me.grpResults.Controls.Add(Me.Label7)
        Me.grpResults.Controls.Add(Me.Label6)
        Me.grpResults.Location = New System.Drawing.Point(15, 143)
        Me.grpResults.Name = "grpResults"
        Me.grpResults.Size = New System.Drawing.Size(299, 82)
        Me.grpResults.TabIndex = 11
        Me.grpResults.TabStop = False
        Me.grpResults.Text = "Results"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(20, 29)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(95, 13)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Total Credit Hours:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(20, 52)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(50, 13)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Average:"
        '
        'btnCalculate
        '
        Me.btnCalculate.Location = New System.Drawing.Point(12, 107)
        Me.btnCalculate.Name = "btnCalculate"
        Me.btnCalculate.Size = New System.Drawing.Size(147, 23)
        Me.btnCalculate.TabIndex = 12
        Me.btnCalculate.Text = "&Calculate"
        Me.btnCalculate.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(167, 107)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(147, 23)
        Me.btnExit.TabIndex = 13
        Me.btnExit.Text = "&Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'lblHours
        '
        Me.lblHours.AutoSize = True
        Me.lblHours.Location = New System.Drawing.Point(243, 29)
        Me.lblHours.Name = "lblHours"
        Me.lblHours.Size = New System.Drawing.Size(42, 13)
        Me.lblHours.TabIndex = 14
        Me.lblHours.Text = "0 hours"
        Me.lblHours.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblGPA
        '
        Me.lblGPA.AutoSize = True
        Me.lblGPA.Location = New System.Drawing.Point(231, 52)
        Me.lblGPA.Name = "lblGPA"
        Me.lblGPA.Size = New System.Drawing.Size(54, 13)
        Me.lblGPA.TabIndex = 15
        Me.lblGPA.Text = "0.00/4.00"
        Me.lblGPA.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cmbSub2Grade
        '
        Me.cmbSub2Grade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSub2Grade.FormattingEnabled = True
        Me.cmbSub2Grade.Items.AddRange(New Object() {"A", "B", "C", "D", "F"})
        Me.cmbSub2Grade.Location = New System.Drawing.Point(70, 53)
        Me.cmbSub2Grade.Name = "cmbSub2Grade"
        Me.cmbSub2Grade.Size = New System.Drawing.Size(121, 21)
        Me.cmbSub2Grade.TabIndex = 14
        '
        'cmbSub3Grade
        '
        Me.cmbSub3Grade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSub3Grade.FormattingEnabled = True
        Me.cmbSub3Grade.Items.AddRange(New Object() {"A", "B", "C", "D", "F"})
        Me.cmbSub3Grade.Location = New System.Drawing.Point(70, 80)
        Me.cmbSub3Grade.Name = "cmbSub3Grade"
        Me.cmbSub3Grade.Size = New System.Drawing.Size(121, 21)
        Me.cmbSub3Grade.TabIndex = 15
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(324, 235)
        Me.Controls.Add(Me.cmbSub3Grade)
        Me.Controls.Add(Me.cmbSub2Grade)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnCalculate)
        Me.Controls.Add(Me.grpResults)
        Me.Controls.Add(Me.txtSub3Hours)
        Me.Controls.Add(Me.txtSub2Hours)
        Me.Controls.Add(Me.txtSub1Hours)
        Me.Controls.Add(Me.cmbSub1Grade)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.Text = "Project: GPA Calculator"
        Me.grpResults.ResumeLayout(False)
        Me.grpResults.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmbSub1Grade As System.Windows.Forms.ComboBox
    Friend WithEvents txtSub1Hours As System.Windows.Forms.TextBox
    Friend WithEvents txtSub2Hours As System.Windows.Forms.TextBox
    Friend WithEvents txtSub3Hours As System.Windows.Forms.TextBox
    Friend WithEvents grpResults As System.Windows.Forms.GroupBox
    Friend WithEvents lblHours As System.Windows.Forms.Label
    Friend WithEvents lblGPA As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnCalculate As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents cmbSub2Grade As System.Windows.Forms.ComboBox
    Friend WithEvents cmbSub3Grade As System.Windows.Forms.ComboBox

End Class
