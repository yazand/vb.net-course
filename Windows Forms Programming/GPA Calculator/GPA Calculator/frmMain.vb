﻿Public Class frmMain

    Private Sub btnCalculate_Click(sender As System.Object, e As System.EventArgs) Handles btnCalculate.Click

        'TotalHours is simply the addition of the contents of each textbox after converting them to integers.
        Dim TotalHours As Integer = CInt(txtSub1Hours.Text) + CInt(txtSub2Hours.Text) + CInt(txtSub3Hours.Text)
        lblHours.Text = TotalHours & " hours" 'show the TotalHours in lblHours

        'GetMark() is a function defined below.
        Dim TotalPoints As Double = GetMark(cmbSub1Grade.Text) * CDbl(txtSub1Hours.Text)
        TotalPoints += GetMark(cmbSub2Grade.Text) * CDbl(txtSub2Hours.Text)
        TotalPoints += GetMark(cmbSub3Grade.Text) * CDbl(txtSub3Hours.Text)

        Dim Average As Double = TotalPoints / TotalHours
        lblGPA.Text = Average.ToString("0.00") & "\4.00"

        'Average.ToString("0.00") formats Average for better output.
        'For example
        '  (14).ToString("0.00") prints 14.00
        '  (2.5).ToString("0.00") prints 2.50
        '  (2.145542).ToString("0.00") prints 2.15
        '  (2.5).ToString("00.00") prints 02.50

    End Sub

    'GetMark() is a function used to convert the textual input (A, B, C, D, F)
    'to its corresponding grade value (4.0, 3.0, 2.0, 1.0, 0.0)
    Function GetMark(ByVal Text As String) As Double
        Select Case UCase(Text)
            Case "A"
                Return 4.0
            Case "B"
                Return 3.0
            Case "C"
                Return 2.0
            Case "D"
                Return 1.0
            Case "F"
                Return 0.0
            Case Else
                MsgBox("Please check your input again.", MsgBoxStyle.Exclamation)
                Return 0
        End Select

    End Function

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Close() 'This closes the current form
    End Sub
End Class
