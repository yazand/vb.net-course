﻿Public Class frmNotepad

    Dim Saved As Boolean = True 'Saved stores whether the current document was saved or not.
    Dim Filepath As String = "Untitled" 'the default filepath is Untitled. This is shown in the title of the form.

    Private Sub mnuFormatFont_Click(sender As System.Object, e As System.EventArgs) Handles mnuFormatFont.Click

        dlgFont.Font = txtText.Font 'like we did for color dialog in class. Check color dialog if you can't remember.
        dlgFont.ShowDialog() 'show the font dialog
        txtText.Font = dlgFont.Font 'change the font

    End Sub

    Private Sub mnuFormatColor_Click(sender As System.Object, e As System.EventArgs) Handles mnuFormatColor.Click

        dlgColor.Color = txtText.ForeColor 'set the dialog's color to the textbox's fore color.
	'This is used because the color is the last one set (i.e. if we changed the back color, the color in the dialog is that of the back color)
        dlgColor.ShowDialog() 'show the color dialog
        txtText.ForeColor = dlgColor.Color 'change text color

    End Sub

    Private Sub mnuFormatBackcolor_Click(sender As System.Object, e As System.EventArgs) Handles mnuFormatBackcolor.Click
	'same as text color
        dlgColor.Color = txtText.BackColor
        dlgColor.ShowDialog()
        txtText.BackColor = dlgColor.Color
    End Sub

    Private Sub mnuFormatWordWrap_Click(sender As System.Object, e As System.EventArgs) Handles mnuFormatWordWrap.Click
        txtText.WordWrap = mnuFormatWordWrap.Checked 'enable/disable word wrap.
    End Sub

    Private Sub mnuFileExit_Click(sender As System.Object, e As System.EventArgs) Handles mnuFileExit.Click
        Close() 
    End Sub

    Private Sub frmNotepad_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

	'make sure the user wants to quit even if he hasn't saved his work.

        If Not Saved Then 
            If MsgBox("Are you sure you want to exit without saving?", MsgBoxStyle.YesNo Or MsgBoxStyle.Question) = MsgBoxResult.No Then e.Cancel = True
        End If

    End Sub

    Private Sub frmNotepad_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        mnuFormatWordWrap.Checked = txtText.WordWrap 'on load, make sure the checked status in the menu for word wrap is set correctly.
    End Sub

    Private Sub txtText_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtText.TextChanged

        Saved = False 'if any text changed, saved is set false.
        Text = "Notepad.NET - " & Filepath & "*" 'add an asterisk at the end of the form's title to signify that the file isn't saved.

        Dim CharsWithoutWhitespace As Integer = 0 'number of characters without whitespace

        For Each C As Char In txtText.Text 'iterate over all characters in txtText.Text
            If Not Char.IsWhiteSpace(C) Then CharsWithoutWhitespace += 1 'check if C is a whitespace or not
        Next

        lblCharacterss.Text = "Characters (without whitespace): " & txtText.TextLength & " (" & CharsWithoutWhitespace & ")"

    End Sub

    Private Sub mnuFileOpen_Click(sender As Object, e As System.EventArgs) Handles mnuFileOpen.Click

	'ask if you want to open a new file without saving the current work
        If Not Saved Then
            If MsgBox("Are you sure you want to load a file without saving your work?", MsgBoxStyle.YesNo Or MsgBoxStyle.Question) = MsgBoxResult.No Then Exit Sub
        End If

        dlgOpen.Filter = "Text file (*.txt)|*.txt" 'only enable opening files ending with .txt
        dlgOpen.Title = "Select a file to open.."

        dlgOpen.CheckFileExists = True

        If dlgOpen.ShowDialog() = Windows.Forms.DialogResult.OK Then 'only open a file if the user clicked Open

            Dim File As String = My.Computer.FileSystem.ReadAllText(dlgOpen.FileName) 'read all textual contents of the file
            txtText.Text = File

            Saved = True

            Filepath = dlgOpen.FileName
            Text = "Notepad.NET - " & dlgOpen.FileName 'show the name of the file in the form's title

        End If

    End Sub

    Private Sub mnuFormatRight_Click(sender As System.Object, e As System.EventArgs) Handles mnuFormatRight.Click
	'because txtText.RightToLeft is not a boolean value, we have to manually set the property by doing a check
        If mnuFormatRight.Checked Then
            txtText.RightToLeft = Windows.Forms.RightToLeft.Yes
        Else
            txtText.RightToLeft = Windows.Forms.RightToLeft.No
        End If
    End Sub

    Private Sub mnuFileSave_Click(sender As System.Object, e As System.EventArgs) Handles mnuFileSave.Click

        dlgSave.AddExtension = True
        dlgSave.DefaultExt = "txt"
        dlgSave.Filter = "Text files (*.txt)|*.txt"
        dlgSave.Title = "Save file"

        If dlgSave.ShowDialog() = Windows.Forms.DialogResult.OK Then
            My.Computer.FileSystem.WriteAllText(dlgSave.FileName, txtText.Text, False)
            Saved = True
            Text = "Notepad.NET - " & dlgSave.FileName
            Filepath = dlgSave.FileName

        End If

    End Sub

    Private Sub mnuFileNew_Click(sender As System.Object, e As System.EventArgs) Handles mnuFileNew.Click

        If Not Saved Then
            If MsgBox("Are you sure you want to create a new file without saving your work?", MsgBoxStyle.YesNo Or MsgBoxStyle.Question) = MsgBoxResult.No Then Exit Sub
        End If

        Filepath = "Untitled"
        txtText.Text = ""
        Saved = False

    End Sub

    'I seem to have forgotten to add frmAbout.
    'to open it, just use:
    '    frmAbout.Show() 'this will enable the user to use both forms at the same time.
    ' or
    '    frmAbout.ShowDialog() 'this will disable the user from using the original form untill frmAbout is closed.

End Class
