﻿Public Class frmSteps

    Private Sub frmSteps_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        'This event fires when the form frmSteps is shown.
        'When frmSteps is shown, I want to show frmStopwatch too.
        frmStopwatch.Show()
    End Sub

End Class
