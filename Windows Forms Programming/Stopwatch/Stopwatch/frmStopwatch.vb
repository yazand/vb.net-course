﻿Public Class frmStopwatch

    Dim NumberOfTicks As Integer = 0 'This variable holds the number of ticks the timer ticked since the start.
    'Remember that the timer's interval is 100 ms. So every tick represents 100 ms.

    Private Sub btnStart_Click(sender As System.Object, e As System.EventArgs) Handles btnStart.Click

        NumberOfTicks = 0
        UpdateTime(0) 'Restart the time.

        btnStart.Enabled = False 'To stop the user from clicking start two times. (It doesn't make sense)
        btnStop.Enabled = True 'Now the user can click on stop. Note: the user can't click on stop before clicking on start.

        tmrWatch.Enabled = True 'tmrWatch.Start() can be used with the same effect.

    End Sub

    Sub UpdateTime(ByVal ElapsedTime As Integer)
        'This subroutine will update the label's text property to reflect the elapsed time.
        'Note that the elapsed time is in milliseconds.

        lblTime.Text = "Elapsed time: " & ElapsedTime / 1000 & " seconds."

    End Sub

    Private Sub tmrWatch_Tick(sender As System.Object, e As System.EventArgs) Handles tmrWatch.Tick

        NumberOfTicks += 1 'Increment NumberOfTicks by one each time that the interval passes.

        Dim Time As Integer = NumberOfTicks * 100 'The time elapsed is equal to 100 ms * number of ticks.
        UpdateTime(Time) 'Update the label.

    End Sub

    Private Sub btnStop_Click(sender As System.Object, e As System.EventArgs) Handles btnStop.Click
        tmrWatch.Enabled = False 'tmrWatch.Stop() can also be used with the same effect.

        btnStart.Enabled = True 'Re-enable clicking on the start button.
        btnStop.Enabled = False 'Disable clicking on the stop button.

    End Sub

End Class