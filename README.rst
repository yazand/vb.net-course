========================================
VB.NET Course @ The University of Jordan
========================================
I will try to keep this code repository updated with our daily work.

 - You can view the source by clicking on the **Source** tab up or going directly using this link http://bitbucket.org/yazand/vb.net-course/src.

 - You can download a source file by clicking on **Raw** after viewing it.

 - You can download all source files by clicking on **Get Source** located on the screen's upper right hand side.

 - I uploaded compressed files for separate Windows Forms projects to make downloading easier.

Please note that the code in this repository is written using Visual Studio 2010 Express. If you are having difficulties opening the projects, please make sure you are using the same version as mine.

For questions or for more information, please either submit an issue
or send an email to yazan.dabain@hotmail.com

Regards,
Yazan S. Dabain

